
from api.response_messages import post_success, client_error, get_success, server_error
from models.rates import Rates


def get_rates(agenda_id=None, query={}):
    try:
        rates = Rates.objects(**query)
        if agenda_id is None:
            return get_success(result=rates)
        else:
            output = rates(agenda_id=agenda_id)

        return get_success(result=output)
    except:
        return client_error(msg=f"rate not found for agenda with agenda_id={agenda_id}")


def post_rate(data):
    """
    Method to post like/dislike of a user on a given agenda
    """
    agenda_id = data["agenda_id"]
    if data["like"] is None:
        return client_error(msg="rate value can't be none")

    try:
        # update user if already rated
        resp = Rates.objects(user_id=data["user_id"], agenda_id=agenda_id).update(**data)
        # if not exist create new one
        if resp == 0:
            Rates(**data).save()

        from helper.h_report import update_agendas_rate_report
        update_agendas_rate_report(agenda_ids=[agenda_id])

        return post_success(result=data)

    except:
        return client_error()


def get_rates_report(agenda_ids=[]):
    """
       method to get total rates ( like, dislike) report of given agenda ids
       if not agenda id given then it will prepare for each agenda ids
       which are existing in rate table
       """
    reports = {}
    if agenda_ids == []:
        all_rates = Rates.objects()
        agenda_ids = set([rate.agenda_id for rate in all_rates])

    for agenda_id in agenda_ids:
        try:
            rates = Rates.objects(agenda_id=agenda_id)
            likes = rates(like=True)
            dislikes = rates(like=False)
            data = {
                "total": len(rates),
                "total_likes": len(likes),
                "total_dislikes": len(dislikes)
            }
            reports[agenda_id]=data
        except:
            continue

    return reports



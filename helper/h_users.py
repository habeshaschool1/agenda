# mongoengine resources_legacy

# project resources_legacy
import json

from flask_jwt_extended import get_jwt_identity, jwt_required, create_access_token, get_raw_jwt

from api.response_messages import post_success, client_error, put_success, get_success, forbidden, server_error
from helper.constants import CUser, PASSWORD_STRENGTH_LEVEL, URL_SIGN_UP_PAGE, URL_SIGN_IN_PAGE
from helper.operate import get_random_int, prepare_access_token, regex_validate, generate_string, send_mail, \
    remove_unnecessary_chars
from models.users import Users
from models.users_new import UsersNew

c_user = CUser()


def get_new_user_id():
    """
    Method to generate a new user ID
     which is not used by any user from users table
    """
    try:
        # prepare a unique user ID
        used_ids = Users.objects().values_list("user_id")
        return get_random_int(used_ids, c_user.USER_MIN_ID, c_user.USER_MAX_ID)
    except:
        return 1001


def post_user(data):
    """
   method to post new user
    """

    # remove unnecessary space and
    # if name is empty then take email part before @ symbol
    name = str(data.get("name", ""))
    email = str(data.get("email", "Anonymous"))
    if len(name) == 0:
        name = email.split("@")[0]

    name = remove_unnecessary_chars(name)
    data["name"] = name

    # if phone is empty take default 11111
    phone = str(data.get("phone", ''))
    if len(phone) == 0:
        data["phone"] = "1111111111"

    if not validate_password(data.get("password", "")):
        msg = "password should be at least 6 chars, and should have at least 1 char from each (lower, Upper, number, special)"
        return client_error(msg=msg)

    # new user should always be normal user
    data["access"] = c_user.ACCESS_NORMAL_USER
    data["activation_key"] = generate_string(number=20, upper=10,
                                             lower=10)  # if it is 1 then account is active or else not active
    post_user = UsersNew(**data)

    try:
        post_user.save()

        message = f"Registration successful, go to your {data['email']} and click the activation link"
        data.pop("password", None)
        activation_url = f"http://localhost:5000/authentication/activate?email={data['email']}&activation_key={data['activation_key']}"
        data.pop("activation_key", None)
        # email activation link to new user
        mail_body = f"""
        Click the link below to activate your agenda account
        {activation_url}
        Copyright Habesha Teck Ltd. 2017 - 2021
        Contact us | (US) (301) 792-9864 | contact@habeshaschool.com
        """
        send_mail(email, body=mail_body)
        return post_success(result=data, msg=message)
    except:
        return client_error(msg="Either missing mandatory field, wrong data type, or record already exist")


def activate_user(email, activation_key):
    """
   method to activate user
   it will get user from UsersNew table
   and move it to Users table
    """
    # check new user is in the users_new table with the given email and activation key
    if email is None or activation_key is None:
        msg = "wrong activation link, make sure you click/copy the link from the email we have sent earlier"
        return client_error(msg=msg)

    try:
        new_user = UsersNew.objects.get(email=email, activation_key=activation_key)
        data = {"user_id": new_user.user_id,
                "email": new_user.email,
                "password": new_user.password,
                "access": new_user.access,
                "name": new_user.name,
                "country_code": new_user.country_code,
                "phone": new_user.phone,
                }

        # post the new user to users table
        post_user = Users(**data)
        post_user.save()
        new_user.delete()
        message = f"Activation successful, now you can start using agenda at {URL_SIGN_IN_PAGE}"
        return post_success(msg=message)
    except:
        return client_error(msg=f"Activation key expired, register again at {URL_SIGN_UP_PAGE}")


def put_user(user_id, data):
    """ method to update user profile including password"""
    if data.get("password", None) is not None:
        if not validate_password(data["password"]):
            msg = "password should be at least 6 chars, 1 lower 1 Upper, 1 number and 1 special characters"
            return client_error(msg=msg)

    user = Users.objects.get(user_id=user_id)

    try:
        put_user = user.update(**data)
        return put_success(result=data)
    except:
        return client_error(result=data)


def change_password(user_id, old_password, new_password):
    """
    method to check if is current user password matches the given old_password
    validate new password if it maches the requirement
    then make a change on user password
    """
    try:
        # check user old_password
        user = Users.objects.get(user_id=user_id)
        if not user.check_pw_hash(old_password):
            return client_error(msg="old password not correct")

        # check new password
        if not validate_password(new_password):
            msg = "password should be at least 6 chars, and should have at least 1 char from each (lower, Upper, number, special)"
            return client_error(msg=msg)

        # change password
        try:
            data = {"password": new_password}
            put_user = user.update(**data)
            return put_success(result=user)
        except:
            return server_error(msg="password not changed, something went wrong, contact admin")
    except:
        return server_error(msg="password not changed, something went wrong, contact admin")


def signin_user(data):
    """ method to check if user has correct login (email, and password)"""
    try:
        email = data.get('email')
        user = Users.objects.get(email=email)
        if not user.active:
            return client_error(
                msg=f"user has not been activated, go to your email {email}  and click activation link sent from contact@agenda.com")

        if user.check_pw_hash(data.get('password')):
            token = prepare_access_token(user.id, user.email, expire_days=30)
            return post_success(result=token, msg="welcome...")
        else:
            return client_error(msg="wrong password")
    except:
        return client_error(msg="wrong email or password")


def refresh_token():
    """ method to refresh the logged in user token"""
    try:
        current_user = get_jwt_identity()
        token = {
            'access_token': create_access_token(identity=current_user)
        }
        return post_success(result=token, msg="refreshed, welcome...")
    except:
        return client_error(msg="something went wrong")


def add_token_to_block_list():
    """ method to add user token to black list"""
    try:
        from app import blacklist

        jti = get_raw_jwt()['jti']
        blacklist.add(jti)
        print(blacklist)
        return post_success(msg="logged out success")
    except:
        return client_error(msg="something went wrong")


def get_users(user_id=None, query={}):
    users = Users.objects()
    current_user = users.get(id=get_jwt_identity())
    try:
        users = users(**query)
        if current_user.access == CUser.ACCESS_ADMIN_USER:
            if user_id is None:
                return get_success(result=users)
            else:
                user = users.get(user_id=user_id)
                return get_success(result=user)

        elif str(current_user.user_id) == user_id:
            user = users.get(user_id=user_id)
            return get_success(result=user)

        else:
            return forbidden(msg="you must be admin to access others user info")
    except:
        return client_error(msg="user/s not found for the given query")


def get_users_public_info(query={}):
    """ this method will show only user name and email
    for public usage """
    users = Users.objects()
    try:
        users = users(**query)
        user_info = []
        for user in users:
            user_info.append({"email": user.email, "name": user.name, "user_id": user.user_id})
        return get_success(result=user_info)
    except:
        return client_error(msg="user/s not found for the given query")


def get_current_user():
    user_info = {"user_id": "",
                 "email": "",
                 "access": "",
                 "name": "",
                 "phone": "",
                 "post_date": ""
                 }

    @jwt_required
    def get_logged_in_user_id():
        """ this method will try to get if there is current user and return the user details"""
        get_jwt_identity()
        current_user = Users.objects.get(id=get_jwt_identity())
        user_info = {"user_id": current_user.user_id,
                     "email": current_user.email,
                     "access": current_user.access,
                     "name": current_user.name,
                     "phone": current_user.phone,
                     "post_date": str(current_user.post_date).split(".")[0]
                     }
        print(user_info)
        return user_info

    try:
        return get_logged_in_user_id()
    except:
        return user_info


def validate_password(password, strength_level=PASSWORD_STRENGTH_LEVEL):
    """
    method to validate password pre-requisite
    if strength level=1 it must have password with no requirement
    if strength level=2 must have >=6 characters
    if strength level=3, should have at least
        >=6 chars
        1 lower
        1 upper
        1 number
        1 sepecial
        """
    if strength_level == 1:
        return len(password) >= 1

    if strength_level == 2:
        return len(password) >= 6

    if len(password) < 6:
        return False
    regex_str = r"(?=.*[a-z]+)(?=.*[A-Z]+)(?=.*[0-9]+)(?=.*[^\w\s]+)"
    return regex_validate(password, regex_str)

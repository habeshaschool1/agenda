# flask packages
import datetime
import os
import re
import random
import string
from configparser import ConfigParser
# external packages
from random import randint

from flask_jwt_extended import create_access_token, create_refresh_token

from helper.constants import IMAGE_EXTENSIONS, ROOT_DIR


def load_constants_file():
    """ this method will read data from config.properties file"""
    const = ConfigParser()
    const.read('../resource/config.properties')
    return const._sections


def get_random_int(exclude=[], min_number=1, max_number=999999):
    """
    method to return a random integer between the two numbers.
    Excluding the given list
    of we are not finding the unique rand int in 40 tries then will return error message
    """
    rand_int = randint(min_number, max_number)
    count = 1
    while rand_int in exclude:
        rand_int = randint(min_number, max_number)
        count = count + 1
        if count > 40:
            return "unable to get a unique random number within 40 tries"

    return rand_int


def prepare_access_token(user_id, user_email, expire_days=5):
    """ this method will prepare access token for a given number
    where it can be used to access resources_legacy
    instead of using usr name and password"""
    expiry = datetime.timedelta(days=expire_days)
    access_token = create_access_token(identity=str(user_id), expires_delta=expiry)
    refresh_token = create_refresh_token(identity=str(user_id))
    token = {'access_token': access_token,
             'refresh_token': refresh_token,
             'logged_in_as': f"{user_email}"
             }
    return token


def find_available_number(exclude_list=[], min_number=1, max_number=1000000):
    """
    this method will find next available number from the given list
    first it will find if any number missed in the list
    if not will give the largest number in the list + 1
    """
    if not exclude_list: return min_number

    exclude_list = list(exclude_list)
    exclude_list.sort()

    # find a missing number in the given sort list
    nex_available_number = None
    candidate = min_number
    for used_id in exclude_list:
        if min_number > used_id: continue
        if used_id == candidate:
            candidate = candidate + 1
            continue
        else:
            nex_available_number = candidate

    # prepare the number
    if nex_available_number is None: nex_available_number = candidate
    if nex_available_number >= max_number: nex_available_number = f"We have crossed the max limit {max_number}"
    return nex_available_number


def validate_image(file_name, allowed_extensions=IMAGE_EXTENSIONS, image_size=None):
    """ this method will take an image
    and validate if extensions are matching our requirement
    and returns either True or False
    """
    allowed_extensions = set(allowed_extensions)
    valid = '.' in file_name and file_name.rsplit('.', 1)[1].lower() in allowed_extensions
    return valid


def validate_image(file_name, allowed_extensions=IMAGE_EXTENSIONS, image_size=None):
    """ this method will take an image
    and validate if extensions are matching our requirement
    and returns either True or False
    """
    allowed_extensions = set(allowed_extensions)
    valid = '.' in file_name and file_name.rsplit('.', 1)[1].lower() in allowed_extensions
    return valid


def create_dir_if_not_exist(root_path=ROOT_DIR, sub_dir=None):
    """ this method will create a directory if it is not exist"""
    if sub_dir is None:
        return "give a sub_dir to create"

    full_dir = os.path.join(root_path, sub_dir)  # folder path

    # create folder if not exits
    if not os.path.isdir(full_dir):
        os.mkdir(full_dir)
    return full_dir


def regex_validate(value_str, regex_str):
    """
    this method will validate if the given string is valid to the given regular expression
    and return true or false
    """
    matched = re.match(regex_str, value_str)
    return bool(matched)


def remove_unnecessary_chars(text):
    """ this method will remove additional spaces and _- from the end if any"""
    text = text.strip()
    text = re.sub("\s\s+", " ", text)
    text = re.sub("[-_]+$", "", text)
    return text


def string_sanitizer(given_text):
    """ this method will generate a unique key from a given text
    will remove all common knowns, adjectives, conjunctions, numbers and spaces
    so that we can use this one as a unique identifier for category and agenda titles """
    given_text = given_text.lower()

    excludes = (" is", " are", " in", " at", " a", " not", " un", " of", " for", " to", " that", " this", "these", " those" ,
                " which", " what", " dislike", " like", " because", " science", "'s", "s ")
    for word in excludes:
        given_text = given_text.replace(word, " ")
    given_text = re.sub("([^a-z]+|[s$])", "", given_text)
    return given_text


def string_validate(given_text, includes=[], excludes=[]):
    """ this method will validate the given string excludes or includes the tiven strings """

    for exclude in excludes:
        if exclude in given_text:
            return False
    for include in includes:
        if include not in given_text:
            return False

    return True


DATE_AGENDA_FORMAT = "%Y-%m-%d %H:%M:%S"
DATE_UTC_FORMAT = "%Y-%m-%d %H:%M:%S: %p UTC"
DATE_GM_FORMAT = "%Y-%m-%d %H:%M:%S:%f"
DATE_TXT_FORMAT = "%Y%m%d%H%M%S%f"


def get_date_from_now(amount, delta="days", format=DATE_AGENDA_FORMAT):
    """
    this method will return the full dat of a dya from now
    if -ve number given then it will give past date
    """
    now = None
    if format == DATE_UTC_FORMAT:
        now = datetime.datetime.utcnow()
    else:
        now = datetime.datetime.now()

    d = datetime.timedelta(days=abs(amount))
    if delta == "hours":
        d = datetime.timedelta(hours=abs(amount))
    elif delta == "minutes":
        d = datetime.timedelta(minutes=abs(amount))
    elif delta == "minutes":
        d = datetime.timedelta(seconds=abs(amount))

    exact_day = None
    if amount > 0:
        exact_day = now + d
    elif amount == 0:
        exact_day = now
    if amount < 0:
        exact_day = now - d
    exact_day = exact_day.strftime(format)
    return exact_day


def compare_two_dates(first_date_str, second_date_str, format=DATE_AGENDA_FORMAT):
    """ this method will compare two dates and return -1 or 0 or 1"""

    print("YYYYYY")
    print(format)
    #todo IT IS FAILING HERE
    return 1
    date1 = datetime(first_date_str)
    date2 = datetime.strptime(second_date_str)
    print("YYYYYYYYYY")
    if date1 < date2:
        return 1
    elif date1 == date2:
        return 0
    else:
        return -1


def generate_string(upper=0, lower=0, number=0, special=0):
    """ this method will generate random string based on the given inputs"""
    lower = [random.choice(string.ascii_lowercase) for _ in range(lower)]
    upper = [random.choice(string.ascii_uppercase) for _ in range(upper)]
    number = [random.choice(string.digits) for _ in range(number)]
    special = [random.choice(string.punctuation) for _ in range(special)]

    final_string = number + special +  lower + upper
    return "".join(final_string)

def send_mail(email_address, subject="agenda activation link", body=""):
    from flask import Flask
    from flask_mail import Mail, Message
    app = Flask(__name__)
    print("__________________________-")
    print(body)
    print("--------------------------")
    mail_settings = {
        "MAIL_SERVER": 'smtp.gmail.com',
        "MAIL_PORT": 465,
        "MAIL_USE_TLS": False,
        "MAIL_USE_SSL": True,
        "MAIL_USERNAME": "habeshaschool@gmail.com", #os.environ['EMAIL_USER'],
        "MAIL_PASSWORD": "AsratG2me." #os.environ['EMAIL_PASSWORD']
    }

    app.config.update(mail_settings)
    mail = Mail(app)
    with app.app_context():
        msg = Message(subject=subject,
                      sender=app.config.get("MAIL_USERNAME"),
                      recipients=[email_address], # replace with your email for testing
                      body=body)
        mail.send(msg)
    return True
# mongoengine resources_legacy

# project resources_legacy
import copy
import json
from datetime import date

from api.response_messages import post_success, client_error, unauthorized, put_success, get_success, not_found, \
    forbidden
from helper.constants import CAgenda
from helper.h_users import get_users_public_info
from helper.operate import find_available_number, remove_unnecessary_chars, string_sanitizer, string_validate
from models.agendas import Agendas
from models.categories import Categories

c_agenda = CAgenda()
agenda_state_change_map = c_agenda.AGENDA_STATE_CHANGE_MAP
excludes = c_agenda.TITLE_EXCLUDES
includes = c_agenda.TITLE_INCLUDES
may_includes = c_agenda.TITLE_MAY_INCLUDES


def get_agendas(agenda_id=None, query={}):
    # remove unnecessary query key values pairs,
    # like 'all' where it is used as a placeholder in ui
    temp = copy.deepcopy(query)
    for key, value in temp.items():
        if str(value).lower() in ["all"]:
            query.pop(key)
        #AD lines to handle empty values in agendas request
        if str(value).lower() in [""]:
            value = "all"
            query.pop(key)

    try:
        agendas = Agendas.objects(**query)
        if agenda_id is None:
            return get_success(result=agendas)
        else:
            result = agendas.get(agenda_id=agenda_id)
            return get_success(result=result)
    except:
        return client_error(msg=f"agenda with agenda_id={agenda_id} not found")


def get_users_who_created_agendas():
    """ this method will get all users who have created agenda/s"""
    resp = get_agendas()
    agendas = json.loads(resp.data)
    resp = get_users_public_info()
    users = json.loads(resp.data)
    user_ids = set([agenda["user_id"] for agenda in agendas["result"]])
    selected_users = []
    for user in users["result"]:
        if user["user_id"] in user_ids:
            selected_users.append(user)
    return get_success(result=selected_users)


def get_states():
    """ this method will return all states to endpoint"""
    states = c_agenda.AGENDA_ALL_STATES_LIST

    return get_success(result=states)


def get_states_map(current_state=None):
    """ this method will return all states with the maps the you could move"""
    states = c_agenda.AGENDA_STATE_CHANGE_MAP
    if current_state is not None:
        try:
            states = states[current_state]
            return get_success(result=states)
        except:
            client_error(result=states, msg=f"no state found = {current_state}")

    return get_success(result=states)


def post_agendas(data):
    # check given data includes category_id
    if data.get("category_id", None) == None:
        return client_error(result=data, msg="category id is required ")

    # remove unnecessary added spaces on title
    if data.get("title", None) is not None:
        data["title"] = remove_unnecessary_chars(data["title"])

        # validate if title contains unnecessary fields
        if not string_validate(data["title"], excludes=excludes, includes=includes):
            result = {"title": data["title"], "excludes": excludes, "includes": includes}
            return client_error(result=result, msg=f"agenda title may included only {may_includes} special chars")

        data["unique_key"] = string_sanitizer(data["title"])

        if data["title"] in ("", " "):
            return client_error(msg="title can't be an empty string")

    try:

        category = Categories.objects(category_id=data["category_id"])
        if not category:
            return not_found(result=data, msg="category not found")

        # prepare new agenda id
        agendas = Agendas.objects
        agenda_ids = agendas.values_list("agenda_id")
        data["agenda_id"] = find_available_number(agenda_ids)

        # post the new agenda and also update report of category
        post_agenda = Agendas(**data)
        post_agenda.save()
        print(data.get("category_id", None))
        from helper.h_report import update_category_report
        update_category_report(category_ids=data.get("category_id", None))
        return post_success(result=data)
    except:
        return client_error(msg="Either missing or wrong data, or Agenda already exists")


def put_agendas(current_user_id, agenda_id, data):
    """
    only owner of agenda will have a right to update
    this method will not allow to update agenda state
    only agendas on state EDIT and REVIEW can be updated
    then update will be done
    """
    current_category_id = 99999  # just a place holder
    new_category_id = data.get("category_id", current_category_id)

    # remove unnecessary added spaces on title
    if data.get("title", None) is not None:
        data["title"] = remove_unnecessary_chars(data["title"])

        # validate if title contains unnecessary fields
        if not string_validate(data["title"], excludes=excludes, includes=includes):
            result = {"title": data["title"], "excludes": excludes, "includes": includes}
            return client_error(result=result, msg=f"agenda title may included only {may_includes} special chars")

        data["unique_key"] = string_sanitizer(data["title"])
        if data["title"] in ("", " "):
            return client_error(msg="title can't be an empty string")

    agenda =None
    try:
        agenda = Agendas.objects.get(agenda_id=agenda_id)
        current_category_id = agenda.category_id
        # you can't change state and category at the same time
        if data.get("state", None) is not None and data.get("category_id", None) is not None:
            return client_error(msg="you can't change agenda 'state' and 'category' at the same time")

    except:
        return not_found(result={"agenda_id": agenda_id})

    # remove if state is passed through update data parameter
    print(data)
    data.pop("state", None)

    # validate if current user is the owner of the agenda
    if current_user_id != agenda.user_id:
        return unauthorized(msg=f"this agenda, agenda_id {agenda_id} is not owned by you")

    # check if new category is going to be set
    if data.get("category_id", None) is not None:
        category = Categories.objects(category_id=data["category_id"])
        if not category:
            return not_found(result=data, msg="category not found")
    # validate if new state is going to be set
    current_state = agenda["state"]
    if current_state not in [c_agenda.AGENDA_STATE_EDIT, c_agenda.AGENDA_STATE_REVIEW]:
        return forbidden(msg="Agendas which are not in EDIT or REVIEW state can't be modified")

    # update agenda and also update category report if category is changed
    agenda.update(**data)
    if new_category_id != current_category_id:
        from helper.h_report import update_category_report
        update_category_report(category_ids=[new_category_id,current_category_id] )

    return put_success(result=Agendas.objects.get(agenda_id=agenda_id))


def change_state(current_user_id, agenda_id, data):
    """
       only owner of agenda will have a right to update
       this method will allow to change agenda state
       changing agenda stat will be done based on the agenda state map rules
       """
    to_state = data.get("state", None)
    # validate given state value
    if to_state is None:
        return client_error(result=c_agenda.AGENDA_ALL_STATES_LIST, msg="missing state field in body, pass one of this")
    if to_state not in c_agenda.AGENDA_ALL_STATES_LIST:
        return client_error(result=c_agenda.AGENDA_ALL_STATES_LIST, msg="incorrect state value, pick one from the list")

    # pick only the state value, don't let user pass other changes while changing state
    data = {"state": to_state}
    agenda = None
    try:
        agenda = Agendas.objects.get(agenda_id=agenda_id)
    except:
        return not_found(result={"agenda_id": agenda_id})

    # validate if current user is the owner of the agenda
    if current_user_id != agenda.user_id:
        return unauthorized(msg=f"this agenda, agenda_id {agenda_id} is not owned by you")

    current_state = agenda["state"]

    # get agenda possible to states
    allowed_states = agenda_state_change_map[current_state]

    # validate if is possible to change state
    if to_state not in allowed_states:
        return forbidden(msg=f"you can't change agenda from '{current_state}' to '{to_state}'")

    # validate agenda based on rules

    # get agenda rules
    rules = None
    msg = "you can't change agenda rules"
    if to_state in c_agenda.AGENDA_STATE_REJECT:
        # no need rule to reject
        pass

    elif current_state in c_agenda.AGENDA_STATE_RATE:
        # agenda rules
        rule_match = agenda["reports"]["rate"]["rule_match"]
        if not rule_match:
            return forbidden(msg="rules are not fulfilled to move state from RATE to FORUM")

    elif current_state in c_agenda.AGENDA_STATE_FORUM:
        # update agenda forum report before validating rules and refresh agenda
        from helper.h_report import update_agendas_forum_report
        update_agendas_forum_report(agenda_ids=[agenda_id])
        agenda = Agendas.objects.get(agenda_id=agenda_id)


        # agenda rules
        rule_match = agenda["reports"]["forum"]["rule_match"]
        if not rule_match:
            return forbidden(msg="rules are not fulfilled to move state from FORUM to VOTE")

    elif current_state in c_agenda.AGENDA_STATE_VOTE:
        rule_match = agenda["reports"]["vote"]["rule_match"]
        if not rule_match:
            return forbidden(msg="rules are not fulfilled to move state from VOTE to CLOSE")

    # update agenda and also update report of that category
    agenda.update(**data)
    from helper.h_report import update_category_report
    update_category_report(category_ids=agenda["category_id"])
    return put_success(result=Agendas.objects.get(agenda_id=agenda_id))


def get_agendas_category_state_report(category_ids=[]):
    """
       method to get total agendas (from each state) report of given categories ids
       if not category id given then it will prepare for each category ids
       which are existing in rate table
       """
    reports = {}
    all_agendas = Agendas.objects()

    for category_id in category_ids:
        data = {
            "total": 0,
            c_agenda.AGENDA_STATE_EDIT: 0,
            c_agenda.AGENDA_STATE_REVIEW: 0,
            c_agenda.AGENDA_STATE_CANCEL: 0,
            c_agenda.AGENDA_STATE_RATE: 0,
            c_agenda.AGENDA_STATE_FORUM: 0,
            c_agenda.AGENDA_STATE_REJECT: 0,
            c_agenda.AGENDA_STATE_VOTE: 0,
            c_agenda.AGENDA_STATE_CLOSE: 0
        }
        try:
            # make counts of each category of agenda
            agendas = all_agendas(category_id=category_id)
            data["total"] = len(agendas)

            for agenda in agendas:
                # increase count for state
                state = agenda.state
                data[state] = data[state] + 1

            reports[category_id] = data
        except:
            continue

    return reports


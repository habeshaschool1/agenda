
from api.response_messages import post_success, client_error, get_success, server_error
from helper.constants import CVote
from models.rates import Rates
from models.votes import Votes
c_vote = CVote()


def get_votes(agenda_id=None, query={}):
    try:
        votes = Votes.objects(**query)
        if agenda_id is None:
            return get_success(result=votes)
        else:
            output = votes.get(agenda_id=agenda_id)

        return get_success(result=output)
    except:
        return client_error(msg=f"vote not found for agenda with agenda_id={agenda_id}")


def get_votes_report(agenda_ids=[]):
    """
    method to get total vote ( yes, no) report of given agenda ids
    if not agenda id given then it will prepare for each agenda ids
    which are existing in votes table
    """
    reports = {}
    if agenda_ids ==[]:
        all_votes = Votes.objects()
        agenda_ids = set([vote.agenda_id for vote in all_votes])
        print(agenda_ids)

    for agenda_id in agenda_ids:
        try:
            votes = Votes.objects(agenda_id=agenda_id)
            yes = votes(vote=c_vote.VOTE_YES)
            no = votes(vote=c_vote.VOTE_NO)
            abstain = votes(vote=c_vote.VOTE_ABSTAIN)
            data = {
                "total": len(votes),
                "total_yes": len(yes),
                "total_no": len(no),
                "total_abstain": len(abstain)
            }
            reports[agenda_id]=data
        except:
            continue

    return reports

import os

URL_HOME_PAGE = "http://127.0.0.1:8888"
URL_SIGN_IN_PAGE = f"{URL_HOME_PAGE}/signin"
URL_SIGN_UP_PAGE = f"{URL_HOME_PAGE}/signup"

ROOT_DIR = os.path.dirname(os.path.abspath("api"))
SECRET = False # if true then user ID will be unknown while voting, to hide who vote what
PASSWORD_STRENGTH_LEVEL = 1 # 1 (must have any value), 2(length should be >=6), 3(must have at least 1 from each (special, upper, number)
IMAGE_EXTENSIONS = ['png', 'jpg', 'jpeg', 'gif']
IMAGE_MAX_SIZE_KB = 300
PROFILE_IMAGE_MAX_SIZE_KB = 200

TITLE_INCLUDES = ""
TITLE_MAY_INCLUDES = '-_?!.:,'
TITLE_EXCLUDES = '!@#$%^&*()+={}\|<>;'

class CAgenda:
    TITLE_EXCLUDES = TITLE_EXCLUDES
    TITLE_INCLUDES = TITLE_INCLUDES
    TITLE_MAY_INCLUDES = TITLE_MAY_INCLUDES

    AGENDA_STATE_EDIT = 'edit'
    AGENDA_STATE_REVIEW = 'review'
    AGENDA_STATE_CANCEL = 'cancel'  # owner wanted to cancel the agenda
    AGENDA_STATE_RATE = 'rate'
    AGENDA_STATE_REJECT = 'reject'  # if agenda is not getting the right quota (51%)
    AGENDA_STATE_FORUM = 'forum'
    AGENDA_STATE_VOTE = 'vote'
    AGENDA_STATE_CLOSE = 'close'
    AGENDA_ALL_STATES_LIST = (AGENDA_STATE_EDIT,
                              AGENDA_STATE_REVIEW,
                              AGENDA_STATE_CANCEL,
                              AGENDA_STATE_RATE,
                              AGENDA_STATE_REJECT,
                              AGENDA_STATE_FORUM,
                              AGENDA_STATE_VOTE,
                              AGENDA_STATE_CLOSE)

    AGENDA_STATE_CHANGE_MAP = {
        AGENDA_STATE_EDIT: [AGENDA_STATE_REVIEW, AGENDA_STATE_CANCEL],
        AGENDA_STATE_REVIEW: [AGENDA_STATE_EDIT, AGENDA_STATE_RATE, AGENDA_STATE_CANCEL],
        AGENDA_STATE_CANCEL: [AGENDA_STATE_EDIT],
        AGENDA_STATE_RATE: [AGENDA_STATE_FORUM, AGENDA_STATE_REJECT],
        AGENDA_STATE_REJECT: [],
        AGENDA_STATE_FORUM: [AGENDA_STATE_VOTE],
        AGENDA_STATE_VOTE: [AGENDA_STATE_CLOSE],
        AGENDA_STATE_CLOSE: []
    }

    def is_state_change_possible(self, current_state, to_state):
        """
        this method will list all possible next states from a given state
        and validate if the given state is able to move to the given next state
        if possible return True or else return false
        """

        allowed_states = self.AGENDA_STATE_CHANGE_MAP[current_state]
        if to_state in allowed_states:
            return True
        else:
            return False

class CUser:
    ACCESS_SYSTEM_USER = 'system'
    ACCESS_ADMIN_USER = 'admin'
    ACCESS_NORMAL_USER = 'user'
    USER_ALL_ACCESS_FLAGS = (ACCESS_SYSTEM_USER, ACCESS_ADMIN_USER, ACCESS_NORMAL_USER)

    USER_MIN_ID = 100000
    USER_MAX_ID = 999999

    # while signing up, will determine the user behaviour active/inactive
    SET_USER_ACTIVE = True


class CCategory:
    MAX_NUMBER_OF_CATEGORIES = 50
    TITLE_EXCLUDES = TITLE_EXCLUDES
    TITLE_INCLUDES = TITLE_INCLUDES
    TITLE_MAY_INCLUDES = TITLE_MAY_INCLUDES


class CVote:
    VOTE_YES = 'yes'
    VOTE_NO = 'no'
    VOTE_ABSTAIN = 'abstain'
    VOTE_ALL_LIST = (VOTE_YES, VOTE_NO, VOTE_ABSTAIN)

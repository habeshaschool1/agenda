# mongoengine resources_legacy

# project resources_legacy
import json
import unicodedata
from datetime import date, datetime

from flask import jsonify

from api.response_messages import post_success, client_error, unauthorized, put_success, not_found, \
    forbidden, get_success, server_error
from helper.constants import CAgenda, CVote
from helper.h_agendas import get_agendas, get_agendas_category_state_report
from helper.h_forums import get_forums_report, get_forums
from helper.h_locks import get_locks
from helper.h_rates import get_rates_report, get_rates
from helper.h_users import get_users, get_users_public_info
from helper.h_votes import get_votes_report, get_votes
from helper.operate import find_available_number, compare_two_dates, get_date_from_now
from models.agendas import Agendas
from models.categories import Categories

c_agenda = CAgenda()
c_vote = CVote()

DASHBOARD_REPORT = {"user_id": 0,
    'owned': {'agendas': [], 'forum': [], 'rate': [], 'reject': [], 'vote': [],
              'close': [], 'review': [], 'edit': [], 'cancel': []}, 'rated': [],
    'like': [], 'dislike': [], 'commented': [], 'total_comments': 0, 'voted': [],
    'yes': [], 'no': [], 'abstain': []}

def validate_rate_rule_match(rules, reports):
    """
    this method will validate whether the current agenda reports match the rul or not
    if it matches it will return True or else it will return False
    """
    # agenda rules
    min_number_of_participants = rules.min_number_of_participants
    like_percent = rules.rate_like_percent

    # agenda current report
    total = reports["total"]
    likes = reports["total_likes"]
    dislikes = reports["total_dislikes"]
    actual_likes_percent = (likes * 100) // total

    if total < min_number_of_participants:
        return False
    if actual_likes_percent < like_percent:
        return False

    return True


def validate_forum_rule_match(rules, reports):
    """
      this method will validate whether the current agenda reports match the rul or not
      if it matches it will return True or else it will return False
    """
    # agenda rules
    min_number_of_participants = rules.min_number_of_participants

    # agenda current reports
    total_comments = reports["total_comments"]
    total_participants = reports["total_participants"]

    if total_participants < min_number_of_participants:
        return False
    return True


def validate_vote_rule_match(rules, reports):
    """
     this method will validate whether the current agenda reports match the rul or not
     if it matches it will return True or else it will return False
    """

    # agenda current rule
    start_date = rules.start_date
    end_date = str(rules.end_date).split(".")[0]

    # agenda current reports
    total = reports["total"]
    total_yes = reports["total_yes"]
    total_no = reports["total_no"]
    total_abstain = reports["total_abstain"]

    current_date = get_date_from_now(0)

    date_diff = compare_two_dates(end_date, current_date)
    if date_diff in [0, 1]:
        return True
    return False


def update_agendas_rate_report_data(agenda_ids=[]):
    """
    method will collect total summary from rate table for the given agenda ids
    and update the given agendas rate report
    if agenda id is not given then it will update all agendas
    """
    reports = get_rates_report(agenda_ids=agenda_ids)
    success_on_agenda_ids = []
    error_on_agenda_ids = []

    for agenda_id, report in reports.items():
        try:
            agenda = Agendas.objects(agenda_id=agenda_id)[0]
            report["rule_match"] = validate_rate_rule_match(agenda.rules.rate, report)
            update_success = agenda.update(set__reports__rate=report)

            if update_success == 1:
                success_on_agenda_ids.append(agenda_id)
            else:
                error_on_agenda_ids.append(agenda_id)

        except:
            error_on_agenda_ids.append(agenda_id)

    result = {"success_agenda_ids": success_on_agenda_ids, "fail_agenda_ids": error_on_agenda_ids}
    return result


def update_agendas_forum_report_data(agenda_ids=[]):
    """
        method will collect total summary from rate table for the given agenda ids
        and update the given agendas rate report
        if agenda id is not given then it will update all agendas
    """
    reports = get_forums_report(agenda_ids=agenda_ids)
    success_on_agenda_ids = []
    error_on_agenda_ids = []
    for agenda_id, report in reports.items():
        try:
            agenda = Agendas.objects(agenda_id=agenda_id)[0]
            report["rule_match"] = validate_forum_rule_match(agenda.rules.forum, report)
            update_success = agenda.update(set__reports__forum=report)

            if update_success == 1:
                success_on_agenda_ids.append(agenda_id)
            else:
                error_on_agenda_ids.append(agenda_id)
        except:
            error_on_agenda_ids.append(agenda_id)

    result = {"success_agenda_ids": success_on_agenda_ids, "fail_agenda_ids": error_on_agenda_ids}
    return result


def update_agendas_vote_report_data(agenda_ids=[]):
    """
        method will collect total summary from rate table for the given agenda ids
        and update the given agendas rate report
        if agenda id is not given then it will update all agendas
    """
    reports = get_votes_report(agenda_ids=agenda_ids)
    success_on_agenda_ids = []
    error_on_agenda_ids = []
    for agenda_id, report in reports.items():
        try:
            agenda = Agendas.objects(agenda_id=agenda_id)[0]
            report["rule_match"] = validate_vote_rule_match(agenda.rules.vote, report)

            update_success = agenda.update(set__reports__vote=report)
            if update_success == 1:
                success_on_agenda_ids.append(agenda_id)
            else:
                error_on_agenda_ids.append(agenda_id)

        except:
            error_on_agenda_ids.append(agenda_id)
    result = {"success_agenda_ids": success_on_agenda_ids, "fail_agenda_ids": error_on_agenda_ids}
    return result


def update_agendas_rate_report(agenda_ids=[]):
    """
    method will collect total summary from rate table for the given agenda ids
    and update the given agendas rate report
    if agenda id is not given then it will update all agendas
    """
    result = update_agendas_rate_report_data(agenda_ids)
    return put_success(result=result, msg="rate agenda report update has been done successfully")


def update_agendas_forum_report(agenda_ids=[]):
    """
        method will collect total summary from rate table for the given agenda ids
        and update the given agendas rate report
        if agenda id is not given then it will update all agendas
    """
    result = update_agendas_forum_report_data(agenda_ids)
    return put_success(result=result, msg="forum agenda report update has been done successfully")


def update_agendas_vote_report(agenda_ids=[]):
    """
        method will collect total summary from rate table for the given agenda ids
        and update the given agendas rate report
        if agenda id is not given then it will update all agendas
    """
    result = update_agendas_vote_report_data(agenda_ids)
    return put_success(result=result, msg="vote agenda report update has been done successfully")


def update_agendas_all_report(agenda_ids=[]):
    """
        method will collect total summary from rate, forum and vote tables
         get summary of each agenda and update the agenda table
    """
    rates = update_agendas_rate_report_data(agenda_ids=agenda_ids)
    forums = update_agendas_forum_report_data(agenda_ids=agenda_ids)
    votes = update_agendas_vote_report_data(agenda_ids=agenda_ids)
    result = {"rates": rates, "forum": forums, "vote": votes}
    return put_success(result=result, msg="all agendas rate, forum and vote report updates has been done successfully")


def update_category_report_data(category_ids=[]):
    """
       this method will aggregate all agendas per category and state
       and update each category with report of total agendas in each state
    """
    if isinstance(category_ids, int):
        category_ids = [category_ids]

    if category_ids == [] or category_ids == "all":
        all_categories = Categories.objects()
        category_ids = set([category.category_id for category in all_categories])

    reports = get_agendas_category_state_report(category_ids=category_ids)
    success_on_category_ids = []
    error_on_category_ids = []

    # for each category report got from agenda table update category table report part
    for category_id, report in reports.items():
        try:
            category = Categories.objects(category_id=category_id)[0]
            update_success = category.update(set__reports=report)

            if update_success == 1:
                success_on_category_ids.append(category_id)
            else:
                error_on_category_ids.append(category_id)

        except:
            error_on_category_ids.append(category_id)

    result = {"success_category_ids": success_on_category_ids, "fail_category_ids": error_on_category_ids}
    return result


def update_category_report(category_ids=[]):
    """
       this method will aggregate all agendas per category and state
       and update each category with report of total agendas in each state
    """
    result = update_category_report_data(category_ids)
    return put_success(result=result, msg="category report update has been done successfully")


def post_agendas(data):
    # check given data includes category_id
    if data.get("category_id", None) == None:
        return client_error(result=data, msg="category id is required ")

    try:

        category = Categories.objects(category_id=data["category_id"])
        if not category:
            return not_found(result=data, msg="category not found")

        # prepare new agenda id
        agendas = Agendas.objects
        agenda_ids = agendas.values_list("agenda_id")
        data["agenda_id"] = find_available_number(agenda_ids)

        post_agenda = Agendas(**data)
        post_agenda.save()
        return post_success(result=data)
    except:
        return client_error(msg="missing field or wrong data type")


def put_agendas_rate(current_user_id, agenda_id, data):
    """
    only owner of agenda will have a right to update it
    validate state transition if any
    invalid data should be validated
    then update will be done
    """
    agenda = None
    try:
        agenda = Agendas.objects.get(agenda_id=agenda_id)

        # you can't change state and category at the same time
        if data.get("state", None) is not None and data.get("category_id", None) is not None:
            return client_error(msg="you cant' change agenda 'state' and 'category' at the same time")

    except:
        return not_found(result={"agenda_id": agenda_id})

    # validate if current user is the owner of the agenda
    if current_user_id == agenda.user_id:
        # check if new category is going to be set
        if data.get("category_id", None) is not None:
            category = Categories.objects(category_id=data["category_id"])
            if not category:
                return not_found(result=data, msg="category not found")
        # validate if new state is going to be set
        if data.get("state", None) is not None:
            current_state = agenda["state"]
            to_state = data["state"]
            if to_state not in c_agenda.AGENDA_ALL_STATES_LIST:
                return client_error(result=c_agenda.AGENDA_ALL_STATES_LIST,
                                    msg="incorrect state value, pick one from the list")
            if not c_agenda.is_state_change_possible(current_state, to_state):
                return forbidden(msg=f"you can't change agenda from '{current_state}'to '{to_state}'")

        # update agenda
        agenda.update(**data)
        return put_success(result=Agendas.objects.get(agenda_id=agenda_id))
    else:
        return unauthorized(msg=f"this agenda, agenda_id {agenda_id} is not owned by you")


def get_agenda_user_rate_report(agenda, user_id):
    """
    this method will take an agenda report and user id
    find the user rate status for the given agenda
    update the agenda report with the user rate status
    and return the updated agenda report
    """
    agenda_id = agenda["agenda_id"]
    rate = get_rates(agenda_id, {"user_id": user_id})
    result = rate["result"]
    agenda["my_input"] = None
    if result is not None:
        agenda["my_input"] = result["like"]
    return agenda


def get_agendas_user_rates_report(agendas, user_id):
    """ this method will accept all agenda results and user id
    and then find what the given user id rate is from rates table
    and update the agenda report with the rate
    then return the updated one"""
    updated_agendas = []
    result = agendas["result"]
    if isinstance(result, list):
        for r in result:
            updated_agenda = get_agenda_user_rate_report(r, user_id)
            updated_agendas.append(updated_agenda)
    else:
        updated_agenda = get_agenda_user_rate_report(result, user_id)
        agendas["result"] = updated_agenda

    return agendas


def get_user_rate_report(user_id, agenda_ids=[]):
    """ for the given agendas or all agendas, this method will collect
    rate(like, dislike),
    forum(tital comments, and each comment detail),
    vote(yes, no or upstain)
    makes one report and return it
    """


def get_user_agenda_report(user_id=None):
    """ this method will get all agendas user has crated"""
    # get rate report
    query = {"user_id": user_id}
    if user_id in [None, ""]: query = {}

    resp = get_agendas(query=query)
    resp = json.loads(resp.data)
    owned = {"agendas":[]}
    # add agenda id in agendas list
    if resp["result"] is None:
        resp["result"] = []

    for r in resp["result"]:
        owned["agendas"].append(r["agenda_id"])
        # add agenda id in each state
        try:
            owned[r["state"]].append(r["agenda_id"])
        except:
            owned[r["state"]] = [r["agenda_id"]]

    summary = {"user_id": user_id, "owned": owned}
    return (summary)


def get_user_rate_report(user_id=None):
    """ for the given agendas or all agendas, this method will collect
    rate(like, dislike),
    makes one report and return it
    """
    # get rate report
    query = {"user_id": user_id}
    if user_id in [None, ""]: query = {}
    resp = get_rates(query=query)
    resp = json.loads(resp.data)
    like = []
    dislike = []
    if resp["result"] is None:
        resp["result"] = []

    for r in resp["result"]:
        if r["like"]:
            like.append(r["agenda_id"])

        else:
            dislike.append(r["agenda_id"])

    summary = {"user_id": user_id, "rated": like + dislike ,"like": like, "dislike": dislike}
    return (summary)


def get_user_forum_report(user_id=None):
    """
    this method will get all comments of a given user and list them
    and also count total comments that the user has made
    """
    # get rate report
    query = {"user_id": user_id}
    if user_id in [None, ""]: query = {}

    resp = get_forums(query=query)
    resp = json.loads(resp.data)
    if resp["result"] is None:
        resp["result"] = []

    print(resp)
    total_comments = len(resp["result"])

    # select distinct agenda ides from the forum table of this user
    temp = {}
    for agenda in resp["result"]:
        temp[int(agenda["agenda_id"])] = 1

    commented =[]
    for agenda_id, value in temp.items():
        commented.append(agenda_id)

    summary = {"user_id": user_id, "commented": commented ,"total_comments": total_comments}
    return (summary)


def get_user_vote_report(user_id=None):
    """ for the given agendas or all agendas, this method will collect
    vote(yes, no, abstain),
    makes one report and return it
    """
    # get votes report
    query = {"user_id": user_id}
    if user_id in [None, ""]: query = {}

    resp = get_votes(query=query)
    resp = json.loads(resp.data)
    votes = []
    yes = []
    no = []
    abstain = []
    if resp["result"] is None:
        resp["result"] = []

    for r in resp["result"]:
        if r["vote"] == c_vote.VOTE_YES:
            yes.append(r["agenda_id"])
        elif r["vote"] == c_vote.VOTE_NO:
            no.append(r["agenda_id"])
        else:
            abstain.append(r["agenda_id"])

    # add all agenda ides that the user has voted too ==> this is important if agenda voting is secret is true
    resp = get_locks(query=query)
    resp = json.loads(resp.data)
    if resp["result"] is None:
        resp["result"] = []

    for r in resp["result"]:
        votes.append(r["agenda_id"])

    summary = {"user_id": user_id, "voted": votes, "yes": yes, "no": no, "abstain": abstain}

    return (summary)

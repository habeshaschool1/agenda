
from api.response_messages import client_error, get_success
from models.forums import Forums


def get_forums(agenda_id=None, query={}):
    if agenda_id is not None:
        query["agenda_id"] = agenda_id
    try:
        forums = Forums.objects(**query)
        return get_success(result=forums)
    except:
        return client_error(msg=f"forum not found for agenda with agenda_id={agenda_id}")


def get_forums_report(agenda_ids=[]):
    """
    method to get total forum ( number of users give comment, total number of comments) of given agenda ids
    if not agenda id given then it will prepare for each agenda ids
    which are existing in forum table
    """
    reports = {}
    if agenda_ids==[]:
        all_forums = Forums.objects()
        agenda_ids = set([forum.agenda_id for forum in all_forums])

    for agenda_id in agenda_ids:
        try:
            # get forum
            forums = Forums.objects(agenda_id=agenda_id)
            user_ids = set([forum.user_id for forum in forums])

            # prepare report data to update
            data = {
                "total_comments": len(forums),
                "total_participants": len(user_ids)
            }
            reports[agenda_id]=data
        except:
            continue

    return reports



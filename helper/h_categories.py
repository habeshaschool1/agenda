# mongoengine resources_legacy

# project resources_legacy
from flask_jwt_extended import get_jwt_identity

from api.response_messages import post_success, client_error, unauthorized, put_success, get_success, not_found, \
    delete_success
from helper.constants import CCategory
from helper.operate import get_random_int, prepare_access_token, find_available_number, remove_unnecessary_chars, \
    string_sanitizer, string_validate
from models.categories import Categories
from models.users import Users

c_category = CCategory()
excludes = c_category.TITLE_EXCLUDES
includes = c_category.TITLE_INCLUDES
may_includes = c_category.TITLE_MAY_INCLUDES

def get_categories(category_id=None, query={}):
    categories = Categories.objects(**query)

    if category_id is None:
        return get_success(result=categories)
    else:
        try:
            category = categories.get(category_id=category_id)
            return get_success(result=category)
        except:
            return not_found(result={"category_id": category_id})


def post_category(data):
    # remove unnecessary added spaces on title
    if data.get("title", None) is not None:
        data["title"] = remove_unnecessary_chars(data["title"])

        # validate if title contains unnecessary fields
        if not string_validate(data["title"], excludes=excludes, includes=includes):
            result = {"title": data["title"], "excludes": excludes, "includes": includes}
            return client_error(result=result,  msg=f"category title may included only {may_includes} special chars")

        data["unique_key"] = string_sanitizer(data["title"])

        if data["title"] in ("", " "):
            return client_error(msg="title can't be an empty string")

    try:
        categories = Categories.objects
        category_pincodes = categories.values_list("category_id")
        data["category_id"] = find_available_number(exclude_list=category_pincodes, max_number=c_category.MAX_NUMBER_OF_CATEGORIES)
        post_category = Categories(**data)
        post_category.save()
        return post_success(result=post_category, msg="category has been created successfully")
    except:
        print(data)
        return client_error(msg="missing data or data type, or category title already exist")


def put_categories(category_id, data):
    try:
        # remove unnecessary added spaces on title
        if data.get("title", None) is not None:
            data["title"] = remove_unnecessary_chars(data["title"])

            # validate if title contains unnecessary fields
            if not string_validate(data["title"], excludes=excludes, includes=includes):
                result = {"title": data["title"], "excludes": excludes, "includes": includes}
                return client_error(result=result, msg=f"category title may included only {may_includes} special chars")

            data["unique_key"] = string_sanitizer(data["title"])

            if data["title"] in ("", " "):
                return client_error(msg="title can't be an empty string")

        category = Categories.objects(category_id=category_id)
        category.update(**data)
        return put_success(result=category)
    except:
        return client_error(msg="something is wrong in your request")


def delete_categories(category_id):
    try:
        category = Categories.objects(category_id=category_id)
        category.delete()
        return delete_success(result=category)
    except:
        return not_found(result=category_id)


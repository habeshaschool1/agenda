
from api.response_messages import post_success, client_error, get_success, server_error
from helper.constants import CVote
from models.locks import Locks
from models.rates import Rates
from models.votes import Votes
c_vote = CVote()


def get_locks(agenda_id=None, query={}):
    try:
        votes = Locks.objects(**query)
        if agenda_id is None:
            return get_success(result=votes)
        else:
            output = votes.get(agenda_id=agenda_id)

        return get_success(result=output)
    except:
        return client_error(msg=f"locks not found for agenda with agenda_id={agenda_id}")


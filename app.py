# flask packages
from flask import Flask, app
from flask_restful import Api
from flask_mongoengine import MongoEngine
from flask_jwt_extended import JWTManager, jwt_required, get_jwt_identity, create_access_token, create_refresh_token, \
    jwt_refresh_token_required, get_raw_jwt

# local packages
from api.routes import create_routes

# external packages
import os
blacklist = set()

# default mongodb configuration
default_config = {'MONGODB_SETTINGS': {
                    'db': 'agenda',
                    'host': 'localhost',
                    'port': 27017,
                    #'username': 'admin',
                    #'password': 'password',
                    'authentication_source': 'None'}, # admin
                  'JWT_SECRET_KEY': 'changeThisKeyFirst'}


def get_flask_app(config: dict = None) -> app.Flask:
    """
    Initializes Flask app with given configuration.
    Main entry point for wsgi (gunicorn) server.
    :param config: Configuration dictionary
    :return: app
    """
    blacklist = set()

    # init flask
    flask_app = Flask(__name__)
    flask_app.url_map.strict_slashes = False

    # configure app
    config = default_config if config is None else config
    flask_app.config.update(config)
    flask_app.config['JWT_SECRET_KEY'] = 'super-secret'  # Change this!
    flask_app.config['JWT_BLACKLIST_ENABLED'] = True
    flask_app.config['JWT_BLACKLIST_TOKEN_CHECKS'] = ['access', 'refresh']
    # load config variables
    if 'MONGODB_URI' in os.environ:
        flask_app.config['MONGODB_SETTINGS'] = {'host': os.environ['MONGODB_URI'],
                                                'retryWrites': False}
    if 'JWT_SECRET_KEY' in os.environ:
        flask_app.config['JWT_SECRET_KEY'] = os.environ['JWT_SECRET_KEY']

    # init api and routes
    api = Api(app=flask_app)
    create_routes(api=api)

    # init mongoengine
    db = MongoEngine(app=flask_app)

    # init jwt manager
    jwt = JWTManager(app=flask_app)
    blacklist = set()

    @jwt.token_in_blacklist_loader
    def check_if_token_in_blacklist(decrypted_token):
        jti = decrypted_token['jti']
        return False
        return jti in blacklist

    return flask_app


if __name__ == '__main__':
    # Main entry point when run in stand-alone mode.

    app = get_flask_app()

    app.run(debug=True)

# mongo-engine packages
from datetime import datetime

from mongoengine import Document, StringField, FloatField, DateTimeField, BooleanField, IntField, EmbeddedDocument, \
    EmbeddedDocumentField, EmailField

from helper.constants import CAgenda
from helper.operate import get_date_from_now

cAgenda = CAgenda()


class RateRule(EmbeddedDocument):
    """
    Custom EmbeddedDocument to set forum rating dat and limit.
    """
    post_date = DateTimeField(default=datetime.now(), help_text='date the student was created')
    start_date = DateTimeField(default=None)
    end_date = DateTimeField(default=None)
    min_number_of_participants = IntField(default=50)
    rate_like_percent = IntField(default=51)


class ForumRule(EmbeddedDocument):
    """
    Custom EmbeddedDocument to set user authorizations.

    :param user: boolean value to signify if user is a user
    :param admin: boolean value to signify if user is an admin
    """
    post_date = DateTimeField(default=datetime.now(), help_text='date the student was created')
    start_date = DateTimeField(default=None)
    end_date = DateTimeField(default=None)
    min_number_of_participants = IntField(default=50)


class VoteRule(EmbeddedDocument):
    """
    Custom EmbeddedDocument to set forum vot start and end date .

    """
    post_date = DateTimeField(default=datetime.now(), help_text='date the student was created')
    start_date = DateTimeField(default=get_date_from_now(30, delta="days"), help_text='date where vote going to start')
    end_date = DateTimeField(default=get_date_from_now(40, delta="days"), help_text='date where vote going to be freeze')


class Rules(EmbeddedDocument):
    """
    This is expected rules that governs agenda state change .
    """
    rate = EmbeddedDocumentField(RateRule, default=RateRule())
    forum = EmbeddedDocumentField(ForumRule, default=ForumRule())
    vote = EmbeddedDocumentField(VoteRule, default=VoteRule())


class RateReport(EmbeddedDocument):
    """
    This is actual report fields about the agenda .
    """
    post_date = DateTimeField(default=datetime.now(), help_text='date the student was created')
    total = IntField(default=0)
    total_likes = IntField(default=0)
    total_dislikes = IntField(default=0)
    rule_match = BooleanField(default=False)


class ForumReport(EmbeddedDocument):
    """
    This is actual report fields about the agenda .
    """
    post_date = DateTimeField(default=datetime.now(), help_text='date the student was created')
    total_comments = IntField(default=0)
    total_participants = IntField(default=0)
    rule_match = BooleanField(default=False)


class VoteReport(EmbeddedDocument):
    """
    This is actual report fields about the agenda .
    """
    post_date = DateTimeField(default=datetime.now(), help_text='date the student was created')
    total = IntField(default=0)
    total_yes = IntField(default=0)
    total_no = IntField(default=0)
    total_abstain = IntField(default=0)
    rule_match = BooleanField(default=False)


class Reports(EmbeddedDocument):
    """
    This is actual report fields about the agenda .
    """
    rate = EmbeddedDocumentField(RateReport, default=RateReport())
    forum = EmbeddedDocumentField(ForumReport, default=ForumReport())
    vote = EmbeddedDocumentField(VoteReport, default=VoteReport())


class Agendas(Document):
    """
    Template for a mongoengine document, which represents a user's favorite meal.

    :param name: required string value
    :param description: optional string value, fewer than 120 characters
    :param price: optional float value
    :param image_url: optional string image url
    :Example:

    >>> import mongoengine
    >>> from app import default_config

    >>> mongoengine.connect(**default_config['MONGODB_SETTINGS'])
    MongoClient(host=['localhost:27017'], document_class=dict, tz_aware=False, connect=True, read_preference=Primary())

    >>> new_meal = Agendas(name= "Vegetable Spring Rolls", \
                        description= "These crisp veggie rolls are filled with"  \
                                     "cabbage, peppers, cucumber, and home-made peanut sauce.")
    >>> new_meal.save()
    <Meal: Meal object>

    """
    agenda_id = IntField(unique=True)
    category_id = IntField(required=True)
    title = StringField(unique=True, required=True, max_length=100)
    unique_key = StringField(unique=True, required=True)
    desc = StringField(required=True, min_length=25)
    state = StringField(required=True, default=cAgenda.AGENDA_STATE_EDIT,
                        options=cAgenda.AGENDA_ALL_STATES_LIST)
    user_id = IntField(required=True)
    email = EmailField(required=True)
    name = StringField(unique=False)
    post_date = DateTimeField(default=datetime.now(), help_text='date the student was created')
    rules = EmbeddedDocumentField(Rules, default=Rules())
    reports = EmbeddedDocumentField(Reports, default=Reports())

    image_url = StringField()

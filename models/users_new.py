# mongo-engine packages
from datetime import datetime

from mongoengine import (Document,
                         EmbeddedDocument,
                         EmbeddedDocumentField,
                         ListField,
                         StringField,
                         EmailField,
                         BooleanField,
                         ReferenceField, IntField, FileField, DateTimeField)

# project resources_legacy
from helper.constants import CUser
from models.agendas import Agendas

# external packages
import re

c_user = CUser()

class Access(EmbeddedDocument):
    """
    Custom EmbeddedDocument to set user authorizations.

    :param user: boolean value to signify if user is a user
    :param admin: boolean value to signify if user is an admin
    """
    user = BooleanField(default=True)
    admin = BooleanField(default=False)


class PhoneField(StringField):
    """
    Custom StringField to verify Phone numbers.

    # Modification of http://regexlib.com/REDetails.aspx?regexp_id=61
    #
    # US Phone number that accept a dot, a space, a dash, a forward slash, between the numbers.
    # Will Accept a 1 or 0 in front. Area Code not necessary
    """
    REGEX = re.compile(r"^\d{6,12}$")

    def validate(self, value):
        # Overwrite StringField validate method to include regex phone number check.
        if not PhoneField.REGEX.match(string=value):
            self.error(f"ERROR: `{value}` Is An Invalid Phone Number. should be number from 6 to 12 digit")
        super(PhoneField, self).validate(value=value)


class UsersNew(Document):
    """
    Template for a mongoengine document, which represents a user.
    Password is automatically hashed before saving.

    :param email: unique required email-string value
    :param password: required string value, longer than 6 characters
    :param access: Access object
    :param fav_meals: List of Meal objects
    :param name: option unique string username
    :param phone: optional string phone-number, must be valid via regex

    :Example:

    >>> import mongoengine
    >>> from app import default_config

    >>> mongoengine.connect(**default_config['MONGODB_SETTINGS'])
    MongoClient(host=['localhost:27017'], document_class=dict, tz_aware=False, connect=True, read_preference=Primary())

    # Create test user
    >>> new_user = Users(email="spam@ham-and-eggs.com", password="hunter2", access={"admin": True})
    >>> new_user.save()
    >>> new_user.name = "spammy"
    >>> new_user.save()

    # Remove test user
    >>> new_user.delete()

    .. seealso:: :class:`Access`, :class:`Phone`, :class:`models.meals.Meals`
    """

    user_id = IntField(unique=True, required=True, min_value=c_user.USER_MIN_ID, max_value=c_user.USER_MAX_ID)
    email = EmailField(required=True, unique=True)
    password = StringField(required=True, regex=None)
    access = StringField(required=True, default=c_user.ACCESS_NORMAL_USER,
                         choices=c_user.USER_ALL_ACCESS_FLAGS)
    name = StringField(unique=False, required=True, max_length=25)
    country_code = StringField(required=True, unique=False, default="001")
    phone = PhoneField(required=True, unique=False)
    activation_key = StringField(unique=False, required=True)
    post_date = DateTimeField(default=datetime.now(), help_text='date the student was created')
    # profile_image = FileField()

    def save(self, *args, **kwargs):
        # Overwrite Document save method to generate password hash prior to saving
        super(UsersNew, self).save(*args, **kwargs)


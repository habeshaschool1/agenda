# mongo-engine packages
from datetime import datetime

from mongoengine import Document, StringField, FloatField, DateTimeField, IntField, EmailField

from helper.constants import CVote, CUser

c_vote = CVote()
c_user = CUser()

class Votes(Document):
    """
    Template for a mongoengine document, which represents a user's favorite meal.

    :param name: required string value
    :param description: optional string value, fewer than 120 characters
    :param price: optional float value
    :param image_url: optional string image url
    :Example:

    >>> import mongoengine
    >>> from app import default_config

    >>> mongoengine.connect(**default_config['MONGODB_SETTINGS'])
    MongoClient(host=['localhost:27017'], document_class=dict, tz_aware=False, connect=True, read_preference=Primary())

    >>> new_meal = Votes(name= "Vegetable Spring Rolls", \
                        description= "These crisp veggie rolls are filled with"  \
                                     "cabbage, peppers, cucumber, and home-made peanut sauce.")
    >>> new_meal.save()
    <Meal: Meal object>

    """

    agenda_id = IntField(required=True)
    vote = StringField(required=True, choices=c_vote.VOTE_ALL_LIST)
    user_id = IntField(required=True, min_value=c_user.USER_MIN_ID, max_value=c_user.USER_MAX_ID)
    pin_code = IntField(unique=True, required=True)
    post_date = DateTimeField(default=datetime.now(), help_text='date the student was created')


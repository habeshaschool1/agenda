# mongo-engine packages
from datetime import datetime

from mongoengine import Document, StringField, IntField, DateTimeField, EmailField, EmbeddedDocumentField, \
    EmbeddedDocument


class Reports(EmbeddedDocument):
    """
    This is actual report fields about the agenda .
    """
    post_date = DateTimeField(default=datetime.now(), help_text='date the student was created')
    total = IntField(default=0)
    edit = IntField(default=0)
    review = IntField(default=0)
    cancel = IntField(default=0)
    rate = IntField(default=0)
    forum = IntField(default=0)
    reject = IntField(default=0)
    vote = IntField(default=0)
    close = IntField(default=0)

class Categories(Document):
    """
    Template for agenda category.
    Each agenda should be under one of the category in category table
    The agenda page will use this category table to group and display categories
    """

    category_id = IntField(required=True, max_value=50)
    title = StringField(required=True, unique=True, max_length=30)
    unique_key = StringField(unique=True, required=True)
    desc = StringField(required=True, min_length=25)
    user_id = IntField(required=True)
    email = EmailField(required=True)
    name = StringField(unique=False)
    post_date = DateTimeField(default=datetime.now(), help_text='date the student was created')
    reports = EmbeddedDocumentField(Reports, default=Reports())


# mongo-engine packages
from datetime import datetime

from flask_jwt_extended import get_jwt_identity
from mongoengine import Document, StringField, FloatField, DateTimeField, EmailField, IntField

from models.users import Users


class Forums(Document):
    """
    Template for a mongoengine document, which represents a user's favorite meal.

    :param name: required string value
    :param description: optional string value, fewer than 120 characters
    :param price: optional float value
    :param image_url: optional string image url
    :Example:

    >>> import mongoengine
    >>> from app import default_config

    >>> mongoengine.connect(**default_config['MONGODB_SETTINGS'])
    MongoClient(host=['localhost:27017'], document_class=dict, tz_aware=False, connect=True, read_preference=Primary())

    >>> new_meal = Agendas(name= "Vegetable Spring Rolls", \
                        description= "These crisp veggie rolls are filled with"  \
                                     "cabbage, peppers, cucumber, and home-made peanut sauce.")
    >>> new_meal.save()
    <Meal: Meal object>

    """

    agenda_id = IntField(required=True)
    user_id = IntField(required=True)
    email = EmailField(required=True)
    name = StringField(unique=False)
    comment = StringField(max_length=240)
    post_date = DateTimeField(default=datetime.now(), help_text='date the student was created')


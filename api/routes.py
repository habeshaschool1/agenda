# flask packages
from flask_restful import Api

# project resources_legacy
from api.category import CategoriesApi
from api.rate import RatesApi
from api.authentication import SignUpApi, SigninApi, ChangePassword, Activate, Refresh, Signout, Signout2
from api.forum import ForumsApi
from api.profile_image import ProfileImagesApi
from api.report import ReportsApi, Dashboard
from api.user import UsersApi
from api.agenda import AgendasApi
from api.vote import VotesApi


def create_routes(api: Api):
    """Adds resources_legacy to the api.

    :param api: Flask-RESTful Api Object

    :Example:

        api.add_resource(HelloWorld, '/', '/hello')
        api.add_resource(Foo, '/foo', endpoint="foo")
        api.add_resource(FooSpecial, '/special/foo', endpoint="foo")


    """
    # user
    api.add_resource(SignUpApi, '/authentication/signup/')
    api.add_resource(Activate, '/authentication/activate')
    api.add_resource(SigninApi, '/authentication/signin/')
    api.add_resource(ChangePassword, '/authentication/change_password/<user_id>')
    api.add_resource(Refresh, '/authentication/refresh/')
    api.add_resource(Signout, '/authentication/signout/')
    api.add_resource(Signout2, '/authentication/signout2/')

    api.add_resource(UsersApi, '/users/', '/users/<user_id>', '/users/public_info', '/current_user')
    api.add_resource(ProfileImagesApi, '/profile_images/', '/profile_image/')

    # category
    api.add_resource(CategoriesApi, '/categories', '/categories/<category_id>')

    # agenda, change state
    api.add_resource(AgendasApi, '/agendas', '/agendas/users', '/agendas/states-list', '/agendas/states-map/<current_state>', '/agendas/<agenda_id>',
                     '/agendas/change-state/<agenda_id>')

    # rate
    api.add_resource(RatesApi, '/rates/', '/rates/<agenda_id>')

    # forum
    api.add_resource(ForumsApi, '/forums/', '/forums/<agenda_id>')

    # vote
    api.add_resource(VotesApi, '/votes/', '/votes/<agenda_id>')

    # report ==> to update each agenda (rate, forum, and vote) counts
    # to get report of given user_id,  if user_id not given then current logged in user id will be taken
    api.add_resource(ReportsApi,'/reports',
                     '/reports/rates/', "/reports/rates/users", "/reports/rates/users/<user_id>",
                     '/reports/forums/', "/reports/forums/users", "/reports/forums/users/<user_id>",
                     '/reports/votes/', "/reports/votes/users", "/reports/votes/users/<user_id>",
                     "/reports/users", "/reports/users/<user_id>",
                     "/reports/categories", "/reports/categories/<category_id>"
                     )

    # to get generic report about agenda (total agendas with state statstics, total likes/dislikes, votes
    api.add_resource(Dashboard, "/dashboard")


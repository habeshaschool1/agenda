# flask packages
from flask import Response, jsonify


def post_success(result=[], msg="posted successfully", status_code=201) -> Response:
    output = {"result": result, "msg": msg}
    resp = jsonify(output)
    resp.status_code = status_code
    return resp


def put_success(result=[], msg="updated successfully", status_code=201) -> Response:
    output = {"result": result, "msg": msg}
    resp = jsonify(output)
    resp.status_code = status_code
    return resp


def delete_success(result=[], msg="deleted successfully", status_code=204) -> Response:
    output = {"result": result, "msg": msg}
    resp = jsonify(output)
    resp.status_code = status_code
    return resp


def get_success(result=[], msg="get successful", status_code=200) -> Response:
    output = {"result": result, "msg": msg}
    resp = jsonify(output)
    resp.status_code = status_code
    return resp


def unauthorized(result=[], msg="not authorized to take this action, follow/ask the business model", status_code=401) -> Response:
    output = {"result": result, "msg": msg}
    resp = jsonify(output)
    resp.status_code = status_code
    return resp


def forbidden(result=[], msg="not authorized to take this action. Follow/ask the business model", status_code=403) -> Response:
    output = {"result": result, "msg": msg}
    resp = jsonify(output)
    resp.status_code = status_code
    return resp


def invalid_rout(result=[], msg="invalid route/uri, check your uri", status_code=400) -> Response:
    output = {"result": result, "msg": msg}
    resp = jsonify(output)
    resp.status_code = status_code
    return resp


def not_found(result=[], msg="the requested resource not found", status_code=404) -> Response:
    output = {"result": result, "msg": msg}
    resp = jsonify(output)
    resp.status_code = status_code
    return resp


def client_error(result=[], msg="something wrong in your request, duplicate value, missing mandatory fields, data already exists", status_code=422) -> Response:
    output = {"result": result, "msg": msg}
    resp = jsonify(output)
    resp.status_code = status_code
    return resp


def server_error(result=[], msg="something wrong on server side", status_code=500) -> Response:
    output = {"result": result, "msg": msg}
    resp = jsonify(output)
    resp.status_code = status_code
    return resp

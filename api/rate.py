# flask packages

from flask import Response, request
from flask_restful import Resource
from flask_jwt_extended import jwt_required, get_jwt_identity

# project resources_legacy
from api.response_messages import forbidden, not_found
from helper.constants import CAgenda
from helper.h_rates import get_rates, post_rate
from models.agendas import Agendas
from models.users import Users

cAgenda = CAgenda()


class RatesApi(Resource):
    """
    Flask-resftul resource for returning db.rate collection.

    :Example:

    >>> from flask import Flask
    >>> from flask_restful import Api
    >>> from app import default_config

    # Create flask app, config, and resftul api, then add VotessApi route
    >>> app = Flask(__name__)
    >>> app.config.update(default_config)
    >>> api = Api(app=app)
    >>> api.add_resource(RatesApi, '/rates/')

    """

    def get(self, agenda_id: int=None) -> Response:
        """
        GET rate of a given agenda.

        :return: JSON object
        """
        args = dict(request.args)
        return get_rates(agenda_id, query=args)

    @jwt_required
    def post(self, agenda_id: int) -> Response:
        """
        POST response method for creating rates.
        JSON Web Token is required.
        Authorization is required: Access(admin=true)

        :return: JSON object
        """
        # prepare data for post
        data = request.get_json(force=True)
        current_user = Users.objects.get(id=get_jwt_identity())
        user_id = current_user.user_id
        data["user_id"] = current_user.user_id
        data["email"] = current_user.email
        data["name"] = current_user.name
        data["agenda_id"] = agenda_id

        like = data.get("like", None)
        agenda = None
        try:
            agenda = Agendas.objects.get(agenda_id=agenda_id)

        except:
            return not_found(msg=f"agenda_id = {agenda_id} is not available")

        if agenda.state != cAgenda.AGENDA_STATE_RATE:
            return forbidden(msg=f"agenda_id= {agenda_id}  is not on 'rate' state to like/dislike for forum")
        return post_rate(data)


# flask packages
from flask import Response, request, jsonify
from flask_restful import Resource
from flask_jwt_extended import jwt_required, get_jwt_identity

# project resources_legacy
from helper import constants
from helper.h_forums import get_forums
from models.agendas import Agendas
from api.response_messages import forbidden, not_found, post_success, get_success
from models.forums import Forums
from models.users import Users

cAgenda = constants.CAgenda()


class ForumsApi(Resource):
    """
    Flask-resftul resource for returning db.forum collection.

    :Example:

    >>> from flask import Flask
    >>> from flask_restful import Api
    >>> from app import default_config

    # Create flask app, config, and resftul api, then add ForumsApi route
    >>> app = Flask(__name__)
    >>> app.config.update(default_config)
    >>> api = Api(app=app)
    >>> api.add_resource(ForumsApi, '/forum/')

    """
    @jwt_required
    def post(self, agenda_id: int) -> Response:
        """
        POST response method for creating forum.
        JSON Web Token is required.
        Authorization is required: Access(admin=true)

        :return: JSON object
        """
        # prepare the data to post
        data = request.get_json(force=True)
        current_user = Users.objects.get(id=get_jwt_identity())
        data["user_id"] = current_user.user_id
        data["email"] = current_user.email
        data["name"] = current_user.name

        data["agenda_id"] = agenda_id

        agenda = None
        try:
            agenda = Agendas.objects.get(agenda_id=agenda_id)

        except:
            return not_found(msg=f"agenda_id = {agenda_id} is not available")

        if agenda.state != cAgenda.AGENDA_STATE_FORUM :
            return forbidden(msg=f"agenda_id= {agenda_id} is not on 'forum' state, to give comment on it")

        agenda = Forums(**data).save()
        return post_success(result=agenda)

    @jwt_required
    def get(self, agenda_id: int=None) -> Response:
        """
        GET response method for single documents in forum collection.

        :return: JSON object
        """
        args = dict(request.args)
        return get_forums(agenda_id=agenda_id, query=args)

# flask packages
from flask import Response, request
from flask_jwt_extended import jwt_required, get_jwt_identity
from flask_restful import Resource

from api.response_messages import forbidden, post_success, delete_success, not_found
# project resources_legacy
from helper.constants import CUser
from helper.h_users import put_user, get_users, get_users_public_info, get_current_user
from helper.operate import get_random_int
from models.users import Users


class UsersApi(Resource):
    """
    Flask-resftul resource for returning db.user collection.

    :Example:

    >>> from flask import Flask
    >>> from flask_restful import Api
    >>> from app import default_config

    # Create flask app, config, and resftul api, then add UsersApi route
    >>> app = Flask(__name__)
    >>> app.config.update(default_config)
    >>> api = Api(app=app)
    >>> api.add_resource(UsersApi, '/user/')

    """

    @jwt_required
    def get(self, user_id: int=None) -> Response:
        """
        GET response method for acquiring single user data.
        JSON Web Token is required.
        Authorization is required: Access(admin=true) or UserId = get_jwt_identity()

        :return: JSON object
        """
        args = dict(request.args)
        if "/users/public_info" in request.url:
            return get_users_public_info(query=args)
        elif "/current_user" in request.url:
            return get_current_user()
        else:
            return get_users(user_id, query=args)

    @jwt_required
    def put(self, user_id: int) -> Response:
        """
        update user
        :return: JSON object
        """
        try:
            users = Users.objects()
            current_user = users.get(id=get_jwt_identity())
            # user profile can be updated either admin user or its own user
            if int(current_user.user_id) == int(user_id) or current_user.access == CUser().ACCESS_ADMIN_USER:
                data = request.get_json(force=True)
                # password can't be modified through here
                data.pop("password", None)
                return put_user(user_id=user_id, data=data)
            else:
                return forbidden(msg=f"you must be owner or admin to update user with user_id ={user_id}")
        except:
            return not_found(msg=f"user_id = {user_id} is not available")

    @jwt_required
    def delete(self, user_id: int) -> Response:
        """
        DELETE response method for deleting user.
        JSON Web Token is required.
        Authorization is required: Access(admin=true)

        :return: JSON object
        """
        current_user = Users.objects.get(id=get_jwt_identity())
        if current_user.access == CUser.ACCESS_ADMIN_USER:
            output = Users.objects(user_id=user_id).delete()
            return delete_success(result=output)
        else:
            return forbidden()

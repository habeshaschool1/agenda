# flask packages

from flask import Response, request, jsonify
from flask_restful import Resource
from flask_jwt_extended import jwt_required, get_jwt_identity

# project resources_legacy
from api.response_messages import forbidden, not_found, get_success, server_error, \
    client_error, post_success
from helper.constants import CAgenda, CUser, SECRET
from helper.h_report import update_agendas_vote_report
from helper.h_votes import get_votes
from helper.operate import get_random_int
from models.agendas import Agendas
from models.locks import Locks
from models.users import Users
from models.votes import Votes

cAgenda = CAgenda()


class VotesApi(Resource):
    """
    Flask-resftul resource for returning db.vote collection.

    :Example:

    >>> from flask import Flask
    >>> from flask_restful import Api
    >>> from app import default_config

    # Create flask app, config, and resftul api, then add VotessApi route
    >>> app = Flask(__name__)
    >>> app.config.update(default_config)
    >>> api = Api(app=app)
    >>> api.add_resource(VotesApi, '/vote/')

    """

    def get(self, agenda_id: int=None) -> Response:
        """
        GET vote of a given agenda.

        :return: JSON object
        """
        args = dict(request.args)
        return get_votes(agenda_id, query=args)

    @jwt_required
    def post(self, agenda_id:int) -> Response:
        """
        POST response method for creating vote.
        JSON Web Token is required.
        Authorization is required: Access(admin=true)

        :return: JSON object
        """
        # prepare data for post
        data = request.get_json(force=True)
        user_id = Users.objects.get(id=get_jwt_identity()).user_id
        data["agenda_id"] = agenda_id

        agenda = None
        try:
            agenda = Agendas.objects.get(agenda_id=agenda_id)

        except:
            return not_found(msg=f"agenda_id = {agenda_id} is not available")

        if not agenda.state == cAgenda.AGENDA_STATE_VOTE:
            return forbidden(result=agenda, msg=f"agenda_id= {agenda_id}  is not in State=vote, to vote ")

        # is user already selected then block to vote
        lock =[]
        try:
            lock = Locks.objects.get(user_id=user_id, agenda_id=agenda_id)
        except:
            server_error()

        if list(lock) != []:
            return not_found(result=lock, msg=f"you already have voted for this agenda")
        try:
            # post user vote
            votes = Votes.objects
            # generate pincode
            used_pincodes = list(votes.values_list("pin_code"))
            data["pin_code"] = get_random_int(used_pincodes, min_number=1000, max_number=9999)
            data["user_id"] = user_id
            if SECRET:
                data["user_id"] = 999999

            resp_vote = Votes(**data).save()

            # post user to lock
            lock_data = {"user_id": user_id, "agenda_id": agenda_id}
            resp = Locks(**lock_data).save()
            message ="You have successfully voted, and PIN code has been given to you, keep in safe for further reference"
            update_agendas_vote_report(agenda_ids=[agenda_id])

            return post_success(result=resp_vote, msg=message)
        except:
            return client_error(msg="Value must be one of ('yes', 'no', 'abstain')")


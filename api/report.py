# flask packages

from flask import Response, request
from flask_jwt_extended import jwt_required, get_jwt_identity
from flask_restful import Resource

from api.response_messages import forbidden, client_error, server_error
# project resources_legacy
from helper.constants import CAgenda, CUser
from helper.h_report import update_agendas_forum_report, update_agendas_vote_report, update_agendas_rate_report, \
    get_user_rate_report, get_user_vote_report, update_agendas_all_report, \
    update_category_report, get_user_agenda_report, get_user_forum_report, DASHBOARD_REPORT
from helper.h_users import get_current_user
from models.users import Users

cAgenda = CAgenda()


class ReportsApi(Resource):
    """
    is to update agenda report ( rate, forum and vote counts)
    only system or admin user is able to update agenda report
    get report of each user (agendas, rates, forums and votes,
    only logged in user can get his/her's report
    or you must be admin to get someones report
    """

    @jwt_required
    def put(self) -> Response:
        url = request.url
        current_user = Users.objects.get(id=get_jwt_identity())
        if current_user.access not in [CUser.ACCESS_ADMIN_USER, CUser.ACCESS_SYSTEM_USER]:
            return forbidden(msg="you must be admin or system user to update agendas report")

        agenda_ids = []
        category_ids = []
        try:
            data = request.get_json(force=True)
            agenda_ids = data["agenda_ids"]
            category_ids = data["category_ids"]
        except:
            pass

        if "/rate" in url:
            return update_agendas_rate_report(agenda_ids=agenda_ids)

        elif "/forum" in url:
            return update_agendas_forum_report(agenda_ids=agenda_ids)

        elif "/vote" in url:
            return update_agendas_vote_report(agenda_ids=agenda_ids)
        elif "/categories" in url:
            return update_category_report(category_ids=category_ids)

        else:
            # make update for agenda (rate, forum and vote reports)
            return update_agendas_all_report(agenda_ids=agenda_ids)

    @jwt_required
    def get(self, user_id: int=None) -> Response:
        url = request.url
        args = dict(request.args)

        # if user id is not set the take current user id
        current_user = get_current_user()

        # each user can get report of it's own
        if user_id is None:
            user_id = current_user["user_id"]
        # only admin user is able to access someone's report
        elif current_user["user_id"] != str(user_id):
            c_user = CUser()
            if current_user["access"] != c_user.ACCESS_ADMIN_USER:
                return forbidden(msg="you must be admin to get another user report")

        # get rate report of the given or logged in user
        if "/rates/users" in url:
            rates = get_user_rate_report(user_id)
            return rates

        # get forum report of the given or logged in user
        if "/forums/users" in url:
            forums = get_user_forum_report(user_id)
            return forums


        # get vote report of the given or logged in user
        elif "/votes/users" in url:
            votes = get_user_vote_report(user_id)
            return votes

        # get all report of the given or logged in user
        elif "/reports/users" in url:
            all = DASHBOARD_REPORT
            all["user_id"] = user_id

            owned = get_user_agenda_report(user_id)
            rates = get_user_rate_report(user_id)
            forums = get_user_forum_report(user_id)
            votes = get_user_vote_report(user_id)

            all.update(owned)
            all.update(rates)
            all.update(forums)
            all.update(votes)
            return all

        # get each user report
        elif "/reports/users/all" in url:
            return server_error(msg="end point is not ready, it is under construction ...")

        else:
            return client_error(msg="url not found", status_code=404)


class Dashboard(Resource):
    """ this report is public to anyone and is to get generic summery about agenda
    total noumber of agendas and counts on each state
    total number of likes, dislikes,
    vote results will be seen here """
    def get(self, user_id: int=None) -> Response:
        url = request.url
        args = dict(request.args)

        # get generic report for dashboard
        if "/dashboard" in url:
            all = DASHBOARD_REPORT

            owned = get_user_agenda_report()
            rates = get_user_rate_report()
            forums = get_user_forum_report()
            votes = get_user_vote_report()

            all.update(owned)
            all.update(rates)
            all.update(forums)
            all.update(votes)
            return all

        else:
            return client_error(msg="url not found", status_code=404)



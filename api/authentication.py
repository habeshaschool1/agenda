# flask packages
from flask import Response, request
from flask_jwt_extended import jwt_required, get_jwt_identity, jwt_refresh_token_required
from flask_restful import Resource

# project resources_legacy
from api.response_messages import not_found, forbidden, client_error, get_success
from helper.constants import CUser
from helper.h_users import get_new_user_id, post_user, signin_user, change_password, put_user, refresh_token, \
    add_token_to_block_list, activate_user
from models.users import Users

class SignUpApi(Resource):
    """
    Flask-resftul resource for creating new user.

    :Example:

    >>> from flask import Flask
    >>> from flask_restful import Api
    >>> from app import default_config

    # Create flask app, config, and resftul api, then add SignUpApi route
    >>> app = Flask(__name__)
    >>> app.config.update(default_config)
    >>> api = Api(app=app)
    >>> api.add_resource(SignUpApi, '/authentication/signup')

    """
    @staticmethod
    def post() -> Response:
        """
        POST response method for creating user.
        :return: JSON object
        """
        data = request.get_json(force=True)
        data["user_id"] = get_new_user_id()
        return post_user(data)


class Activate(Resource):
    """
       this method will activate user account
    """
    @staticmethod
    def get() -> Response:
        """
       after registration activation key will work only for the next 24 hours
       after that activation key might no work
       """
        email = request.args.get("email", None)
        activation_key = request.args.get("activation_key", None)
        return activate_user(email, activation_key)


class SigninApi(Resource):
    """
        method to login to your email and password
    """
    @staticmethod
    def post() -> Response:
        """
        POST response method for retrieving user web token.
        :return: JSON object
        """
        # check usr name and password
        data = request.get_json(force=True)
        return signin_user(data)


class ChangePassword(Resource):
    """
        method to change user password
        you must be either admin user or
        owner of the logged in user
    """
    @staticmethod
    @jwt_required
    def put(user_id: int) -> Response:
        """
        only admin user or owner of the account may change password
        chek if user is either admin or owner of the account
        check old password matches
        check new password fulfil the requirement
        change password"""
        try:
            # check user is admin or owner of the given user_id
            users = Users.objects()
            current_user = users.get(id=get_jwt_identity())
            # user profile can be updated either admin user or its own user
            if current_user.user_id == user_id or current_user.access == CUser().ACCESS_ADMIN_USER:
                data = request.get_json(force=True)
                return change_password(user_id, data.get("old_password", ""), data.get("new_password", ""))
            else:
                return forbidden(msg=f"you must be owner or admin to change password of user_id ={user_id}")
        except:
            return not_found(msg=f"user_id = {user_id} is not available")


class Refresh(Resource):
    @staticmethod
    @jwt_refresh_token_required
    def post() -> Response:
        return refresh_token()


class Signout(Resource):
    @staticmethod
    @jwt_required
    def delete() -> Response:
        return add_token_to_block_list()


class Signout2(Resource):
    @staticmethod
    @jwt_refresh_token_required
    def delete() -> Response:
        return add_token_to_block_list()



import os

from flask import Flask, request, Response, send_file
from flask_jwt_extended import jwt_required, get_jwt_identity
from flask_restful import Resource

from api.response_messages import server_error, client_error, \
    not_found, post_success
from helper.constants import IMAGE_EXTENSIONS
from helper.operate import create_dir_if_not_exist, validate_image
from models.users import Users
profile_images_dir = "profile_images"
target = create_dir_if_not_exist(sub_dir=profile_images_dir)
image_full_path = os.path.join(target, r"{}.JPG")


class ProfileImagesApi(Resource):

    @jwt_required
    def post(self) -> Response:
        """
        to get user profile image and save it on profile_images dir
        image name will be user id.JPEG
        """
        user_id = Users.objects.get(id=get_jwt_identity()).user_id
        create_dir_if_not_exist()
        image = None
        try:
            image = request.files['image']
        except:
            client_error(msg="image required")

        image = request.files["image"]
        if not validate_image(image.filename):
            return client_error(msg=f"make sure image is of type: {str(IMAGE_EXTENSIONS)}")

        try:
            final_full_path = image_full_path.format(str(user_id))
            image.save(final_full_path)
            return post_success(result={"image_pat": final_full_path}, msg=f"image has been saved successfully")

        except:
            server_error(msg="unable to upload the file, ask your administrators")

    @jwt_required
    def get(self) -> Response:
        user_id = Users.objects.get(id=get_jwt_identity()).user_id
        try:
            return send_file(image_full_path.format(str(user_id)))
        except:
            not_found()







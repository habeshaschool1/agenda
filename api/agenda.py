# flask packages

from flask import Response, request
from flask_jwt_extended import jwt_required, get_jwt_identity
from flask_restful import Resource

from helper.constants import CAgenda
# project resources_legacy
from helper.h_agendas import post_agendas, get_agendas, put_agendas, change_state, get_states, get_states_map, \
    get_users_who_created_agendas
from models.users import Users

cAgenda = CAgenda()


class AgendasApi(Resource):
    """
    Flask-resftul resource for returning db.agenda collection.
    """

    @jwt_required
    def post(self) -> Response:
        current_user = Users.objects.get(id=get_jwt_identity())
        # get new agenda_id
        data = request.get_json(force=True)
        data["user_id"] = current_user.user_id
        data["email"] = current_user.email
        data["name"] = current_user.name
        data["state"] = cAgenda.AGENDA_STATE_EDIT
        return post_agendas(data)

    def get(self, agenda_id: int = None, current_state: str = None) -> Response:
        """
        GET response method for single documents in agenda collection.

        :return: JSON object
        """
        url = request.url
        if "/states-list" in url:
            return get_states()
        elif "/states-map" in url:
            return get_states_map(current_state=current_state)
        elif "/agendas/users" in url:
            rates = get_users_who_created_agendas()
            return rates

        else:
            args = dict(request.args)
            return get_agendas(agenda_id, query=args)

    @jwt_required
    def put(self, agenda_id: int) -> Response:
        """
        only owner of agenda has the right to edit agendas
        updating agenda should not include change state
        change state should be done separately
        """
        data = request.get_json(force=True)
        current_user_id = Users.objects().get(id=get_jwt_identity()).user_id
        if "/change-state" in request.url:
            return change_state(current_user_id, agenda_id, data)
        else:
            return put_agendas(current_user_id, agenda_id, data)




from flask import Flask, request, Response
from flask_jwt_extended import jwt_required, get_jwt_identity
from flask_restful import Resource

from api.response_messages import not_found, client_error
from helper.db import pymongo
from helper.operate import validate_image
from models.users import Users
profile_image_collection = "profile_images"


class FileLoader(Resource):

    @jwt_required
    def post(self) -> Response:
        users = Users.objects()
        current_user = users.get(id=get_jwt_identity())
        user_id = str(current_user.user_id)

        file = None
        try:
            file = request.files['file']
        except:
            return not_found()

        if not validate_image(file, file.filename):
            return client_error(msg="image not valid")
        pymongo.save_file(filename=user_id, fileobj=file, base=profile_image_collection)
        return pymongo.send_file(filename=user_id, base=profile_image_collection)

    @jwt_required
    def get(self) -> Response:
        users = Users.objects()
        current_user = users.get(id=get_jwt_identity())
        user_id = str(current_user.user_id)

        return pymongo.send_file(filename=user_id, base=profile_image_collection)





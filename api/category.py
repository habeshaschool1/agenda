# flask packages
from flask import Response, request
from flask_jwt_extended import jwt_required, get_jwt_identity
from flask_restful import Resource

from api.response_messages import forbidden
# project resources_legacy
from helper.h_categories import get_categories, put_categories, post_category, delete_categories
from helper.constants import CUser
from models.users import Users

cuser = CUser()


class CategoriesApi(Resource):
    def get(self, category_id: int = None) -> Response:
        args = dict(request.args)
        return get_categories(category_id=category_id, query=args)

    @jwt_required
    def put(self, category_id: int) -> Response:
        current_user = Users.objects.get(id=get_jwt_identity())
        if current_user.access == cuser.ACCESS_ADMIN_USER:
            data = request.get_json(force=True)
            return put_categories(category_id, data)
        else:
            return forbidden(msg="you must be admin to update category")

    @jwt_required
    def post(self) -> Response:
        current_user = Users.objects.get(id=get_jwt_identity())
        if current_user.access == cuser.ACCESS_ADMIN_USER:
            data = request.get_json(force=True)
            data["user_id"] = current_user.user_id
            data["email"] = current_user.email
            data["name"] = current_user.name
            return post_category(data)
        else:
            return forbidden(msg="you must be admin to create category")

    @jwt_required
    def delete(self, category_id: int) -> Response:
        current_user = Users.objects.get(id=get_jwt_identity())
        if current_user.access == cuser.ACCESS_ADMIN_USER:
            return delete_categories(category_id)
        else:
            return forbidden(msg="you must be admin to delete category")

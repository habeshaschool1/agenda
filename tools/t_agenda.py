from datetime import datetime
import json

from mongoengine import NotUniqueError, ValidationError

# project resources_legacy
from helper.constants import CVote
from helper.h_forums import get_forums_report
from models.agendas import Agendas
from models.forums import Forums
from models.rates import Rates
from models.users import Users
from models.votes import Votes
from tools.mongo_loader import mongo

c_vote = CVote()


@mongo
def update_reports_rate(agenda_id):
    """
    this method will get total rate summary (likes, dislikes)
    and update the agenda table with total rates, total likes and total dislikes
    """

    from helper.h_rates import get_rates_report
    data = get_rates_report(agenda_id)
    return_str = "rate report update success"
    try:
        Agendas.objects(agenda_id=agenda_id).update(set__reports__rate=data)
    except:
        return_str = "rate report update not success"
    print(return_str)
    return return_str
@mongo
def update_reports_forum(agenda_id):
    """
    this method will get total participants to the agenda forum
     number of comments has been given
    """
    data = get_forums_report(agenda_id)
    return_str = "forum report update success"
    try:
        Agendas.objects(agenda_id=agenda_id).update(set__reports__forum=data)
    except:
        return_str = "forum report update not success"
    print(return_str)
    return return_str
@mongo
def update_reports_vote(agenda_id):
    """
    method to get total vote ( yes, no) report
    and update agenda table
    """

    votes = Votes.objects(agenda_id=agenda_id)
    yes = votes(vote=c_vote.VOTE_YES)
    no = votes(vote=c_vote.VOTE_NO)
    abstain = votes(vote=c_vote.VOTE_ABSTAIN)
    data = {
        "total": len(votes),
        "total_yes": len(yes),
        "total_no": len(no),
        "total_abstain": len(abstain)
    }
    return_str = "vote report update success"
    try:
        Agendas.objects(agenda_id=agenda_id).update(set__reports__vote=data)
    except:
        return_str = "vote report update not success"
    print(return_str)
    return return_str

update_reports_forum(1)